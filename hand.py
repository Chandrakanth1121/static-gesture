import cv2
import tensorflow as tf
import tensorflow.keras
import numpy as np
import time
import pandas as pd
from tensorflow.keras import backend as k
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Activation,Dropout,Flatten,Dense
from tensorflow.python.keras.layers import Conv2D,MaxPooling2D
from tensorflow.python.keras.layers.core import Dense,Flatten
from keras.optimizers import adam
from tensorflow.python.keras.metrics import categorical_crossentropy
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.preprocessing import image
#from tensorflow.keras import *
from tensorflow.python.keras.layers.normalization import BatchNormalization
from tensorflow.python.keras.layers.convolutional import *
from matplotlib import pyplot as plt
from tensorflow.python.keras import backend as K
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt
import keras


cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))
i=500
while (cap.isOpened()):
    i+=1
    cv2.waitKey(50)
    ret, frame= cap.read()
    frame=cv2.flip(frame,1)
    #cv2.rectangle(frame,(0,0),(300,400),(225,225,225),3)
    if ret == True:
        #gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY
        frame=cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        fr = cv2.GaussianBlur(frame, (25,25),cv2.BORDER_DEFAULT)
        lower = np.array([0, 0.23 * 255, 50], dtype = "int")
        upper = np.array([50, 0.68 * 255, 255], dtype = "int")
        skinMask = cv2.inRange(fr, lower, upper)
        #ret,thresh=cv2.threshold(fr,170,255,cv2.THRESH_BINARY_INV)
        contours,hierarchy=cv2.findContours(skinMask,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        #print(len(contours))
        #if(len(contours)==0):
        #    continue
        if(len(contours)==0):
            continue;
        c = max(contours, key=cv2.contourArea)
        extLeft = tuple(c[c[:, :, 0].argmin()][0])
        extRight = tuple(c[c[:, :, 0].argmax()][0])
        extTop = tuple(c[c[:, :, 1].argmin()][0])
        extBot = tuple(c[c[:, :, 1].argmax()][0])
        boxes =[]
        (x,y,w,h)=cv2.boundingRect(c)
        boxes.append([x, y, x + w, y + h])
        boxes=np.array(boxes)
        cv2.rectangle(frame, (boxes[0][0],boxes[0][1]), (boxes[0][2],boxes[0][3]), (225, 225, 225), 3)
        roi=skinMask[boxes[0][1]:boxes[0][3]+10,boxes[0][0]:boxes[0][2]+10]
        if(len(roi)==0):
            continue;
        #print(len(c))
        hull=[cv2.convexHull(c)]
        #areahull=cv2.contourArea(hull[0])
        #areacontour=cv2.contourArea(c)
        #r=((areahull-areacontour)/areacontour)*100
        #print(r)
        cv2.drawContours(frame,hull,-1,(0,255,0),3)
        #cv2.imshow('thresh',thresh)
        font=cv2.FONT_HERSHEY_COMPLEX

        #if r>25:
        #    cv2.putText(frame,'open hand',(5,50),font,1,(0,0,255),2)
        #elif r>47 and r<51:
        #    cv2.putText(frame, '2', (5, 50), font, 1, (0, 0, 255), 2)
        #else:
        #    cv2.putText(frame, 'fist', (5, 50), font, 1, (0, 0, 255), 2)
        #print(frame[200][200])
        cv2.putText(frame,'.',extBot,font,3,(255,255,255),2)
        cv2.putText(frame, '.',extTop, font, 3, (255, 255, 255), 2)
        cv2.putText(frame, '.',extRight, font, 3, (255, 255, 255), 2)
        cv2.putText(frame, '.',extLeft, font, 3, (255, 255, 255), 2)
        #cv2.putText(frame, '+', (boxes[0][0],boxes[0][1]), font, 3, (255, 255, 255), 2)
        #cv2.putText(frame, '+', (boxes[0][2],boxes[0][3]), font, 3, (255, 255, 255), 2)
        out.write(frame)
        cv2.putText(frame, '.', (200,200), font, 3, (255, 255, 255), 2)
        #if(len(roi)!=0):
        #    cv2.imwrite('/home/chandrakanth/Desktop/opencv/opencv-projects/hand/thumbs_down/td'+str(i)+'.jpg',roi)

        new_model = tf.keras.models.load_model('gestures.h5')
        cv2.imwrite('pred.jpg',roi)
        img_pred = image.load_img('pred.jpg', target_size=(150, 150))
        img_pred = image.img_to_array(img_pred)
        img_pred = np.expand_dims(img_pred, axis=0)
        rslt = new_model.predict(img_pred)
        arr = np.rint(rslt)
        #print(arr)
        #print(rslt)
        if (arr[0][0] == 1):
            continue;
        elif(arr[0][1]==1):
            print('open hand')
        elif (arr[0][2] == 1):
            print('thumbs down')
        else:
            print('thumbs up')
        cv2.imshow('',frame)
        #cv2.imshow('',fr)
        #cv2.imshow('video',skinMask)
        #if(roi):
        cv2.imshow('video', roi)
        #else:
        #    continue;
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
